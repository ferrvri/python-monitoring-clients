import socket
import sys
import os
import ipaddress


def connect(ip, port):
	try:
		s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		if s.connect_ex((ip, port)) == 0:
			while True:
				cmd = raw_input('Server >')
				if cmd[:len('exit')] == 'exit':
					break
				elif cmd[:len('disconnect')] == 'disconnect':
					s.send('disconnect')
					print 'Disconnect from the host'
				elif cmd[:len('upload')] == 'upload':
					s.send('upload')
					d = s.recv(1024)
					if not d: break
					if d[:len('ready')] == 'ready':
						path = raw_input('PATH>')
						to = raw_input('TO>')
						s.send('path,{}'.format(to))
						print 'Writing'
						f = open(path, 'rb')
						l = f.read(1024)
						while (l):
							print 'Uploading...'
							s.send(l)
							l = f.read(1024)
						f.close()
						s.close()
						print 'Done. reconnecting...'
						connect(host, port)
						break
				elif cmd[:len('copy')] == 'copy':
					s.send('copy')
					d = s.recv(1024)
					if d == 'ready':
						path = raw_input('TO COPY>')
						dest = raw_input('DESTINY>')
						s.send('path,{}'.format(path))
						f = open(dest, 'wb')
						d = s.recv(1024)
						while (d):
							print 'Receiving...'
							f.write(d)
							d = s.recv(1024)
						f.close()
						s.close()
						print 'Done. reconnecting...'
						connect(host, port)
						break
				else:
					s.send(cmd)
					data = s.recv(4200)
					if not data: break;
					print data
	except socket.error as e:
		print e

def scan(ip, port):
	s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	socket.setdefaulttimeout(.10)
	net = ipaddress.ip_network(unicode(ip))
	all_hosts = list(net.hosts())
	for i in range(len(all_hosts)):
		print 'Host -> {}'.format(unicode(all_hosts[i]))
		result = s.connect_ex( (unicode(all_hosts[i]), port) )
		if result == 0:
			print 'Found -> {}'.format(unicode(all_hosts[i]))
			s.close()
		else:
			print 'Nope'

def scanone(ip, port):
	print 'Trying to connect'
	s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	socket.setdefaulttimeout(None)
	if s.connect_ex( (ip, port) ) == 0:
		print 'Found! connect to and have fun :)'
		s.close()
host = ''
port = 0
for a in range(len(sys.argv)):
	if sys.argv[a] == '-c':
		host = sys.argv[a+1]; port = int(sys.argv[a+2])
		connect(sys.argv[a+1], int(sys.argv[a+2]))
	elif sys.argv[a] == '-sa':
		scan(sys.argv[a+1], int(sys.argv[a+2]))
	elif sys.argv[a] == '-so':
		scanone(sys.argv[a+1], int(sys.argv[a+2]))
	elif len(sys.argv) <= 2:
		print 'Wrong usage'
